/********************************************************************************
 * File Name: 17_DC_MOTOR_WITH_L293D_IC.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/9
 * 
 * Last Modified On: 2020/7/9
 * 
 * Description: Test DC Motor with L293D Motor Driver IC.
 *
 ********************************************************************************/

#define ENABLE_PIN    5      /* Enable PIN of L293D IC connected to Arduino */
#define IN_1_PIN      4      /* In 1 PIN of L293D IC connected to Arduino */
#define IN_2_PIN      3      /* In 2 PIN of L293D IC connected to Arduino */

#define MAX_SPEED     255    /* DC Motor maximum speed */
#define MIN_SPEED     0      /* DC Motor minimum speed */

void setup() {
  pinMode( ENABLE_PIN, OUTPUT );
  pinMode( IN_1_PIN,   OUTPUT );
  pinMode( IN_2_PIN,   OUTPUT );

  Serial.begin( 9600 );
}

void loop() {
  /* Turn the motor in clockwise direction for 5 seconds */
  analogWrite( ENABLE_PIN, MAX_SPEED / 2 ); /* Enable speed control pin with half full speed */
  digitalWrite( IN_1_PIN, LOW );
  digitalWrite( IN_2_PIN, HIGH );
  delay( 5000 );

  /* Stop the motor for 2 seconds */
  analogWrite( ENABLE_PIN, MIN_SPEED );
  delay( 2000 );
  
  /* Turn the motor in counter-clockwise direction for 5 seconds */
  analogWrite( ENABLE_PIN, MAX_SPEED ); /* Enable speed control pin with full speed */
  digitalWrite( IN_1_PIN, HIGH );
  digitalWrite( IN_2_PIN, LOW );
  delay( 5000 );

  /* Stop the motor for 2 seconds */
  analogWrite( ENABLE_PIN, MIN_SPEED );
  delay( 2000 );

}
