/********************************************************************************
 * File Name: 9_MAX7219_LED_DOT_MATRIX.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/5
 * 
 * Last Modified On: 2020/7/5
 * 
 * Description: Test MAX7219 LED dot matrix module. Display (draw) '1', '2', '3'
 *              character to the 8x8 LED dot matrix module.
 * 
 ********************************************************************************/

#include "LedControl.h"

/* DEFINE PINS FOR LED DOT MATRIX MODULE*/
#define DATA_IN_PIN     12    /* Pin 12 is connected to the DataIn  */
#define CS_PIN          11    /* Pin 11 is connected to LOAD(CS) */
#define CLK_PIN         10    /* Pin 10 is connected to the CLK  */

#define LED_MTX_NUM     1     /* Single MAX72XX led dot matrix module */
#define LED_MTX_BRI     1     /* Led dot matrix module brightness */
#define LED_MTX_SIZE    8     /* Led dot matrix module size (8x8) */

/* Create LedControl instance */
LedControl lc = LedControl( DATA_IN_PIN, CLK_PIN, CS_PIN, LED_MTX_NUM );

/* Switching time */
unsigned long delay_time = 1000;

/* '1' character ( Format: 'Bxxxxxxxxx' (8 bits): bit 1 -> LED ON, bit 0 -> LED 0FF) */
byte char_1[ LED_MTX_SIZE ] = {
                                 B00011000,    /* Row 1 */
                                 B00111000,    /* Row 2 */
                                 B01011000,    /* Row 3 */
                                 B00011000,    /* Row 4 */
                                 B00011000,    /* Row 5 */
                                 B00011000,    /* Row 6 */
                                 B00011000,    /* Row 7 */
                                 B01111110     /* Row 8 */
                              };

/* '2' character */
byte char_2[ LED_MTX_SIZE ] = { B00111100, B01000010, B00000100, B00001000, B00010000, B00100000, B01000000, B01111110 };

/* '3' character */
byte char_3[ LED_MTX_SIZE ] = { B00111100, B01000010, B00000100, B00011000, B00000100, B00000010, B01000100, B00111000 };

void setup()
{
  /* The MAX72XX module is in power-saving mode on startup 
     -> have to do a wakeup call */
  lc.shutdown( 0, false );
  
  /* Set the brightness of led dot matrix */
  lc.setIntensity( 0, LED_MTX_BRI );
  
  /* Clear the display */
  lc.clearDisplay( 0 );
}

/********************************************************************************
 * Function Name: void draw_img( byte img[] )
 * 
 * Parameters: byte img[]   :  image bitmap
 * 
 * Return: None.
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/5
 * 
 * Last Modified On: 2020/7/5
 * 
 * Description: Draw an image with LED dot matrix module.
 * 
 ********************************************************************************/
void draw_img( byte img[] )
{
  for ( int row = 0 ; row < LED_MTX_SIZE ; row ++ )
  {
    lc.setRow( 0, row, img[ row ] );
  }
}

void loop()
{
  draw_img( char_1 );
  delay( delay_time );
  draw_img( char_2 );
  delay( delay_time );
  draw_img( char_3 );
  delay( delay_time );
}
