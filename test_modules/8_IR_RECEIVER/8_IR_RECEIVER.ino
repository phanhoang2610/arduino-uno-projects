/********************************************************************************
 * File Name: 8_IR_RECEIVER.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/30
 * 
 * Last Modified On: 2020/6/30
 * 
 * Description: Test IR Receiver module (with remote control). When presses the
 *              POWER button on remote control, turn the LED on or off. Press
 *              up/down button on remote control to increase/decrease the
 *              LED brightness.
 * 
 ********************************************************************************/

#include <IRremote.h>

#define IR_RECV_PIN       11          /* Digital pin connected to IR receiver output */

/* Elegoo Remote Control's Buttons RF Hex Code */
#define OFF_BTN           0xFFA25D    /* OFF Button        */
#define VOL_UP_BTN        0xFF629D    /* Volume up Button  */
#define VOL_DOWN_BTN      0xFFA857    /* Vol Down Button   */
#define FUNC_STOP_BTN     0xFFE21D    /* Func/Stop Button  */
#define BACKWARD_BTN      0xFF22DD    /* Backward Button   */
#define PLAY_PAUSE_BTN    0xFF02FD    /* Play/Pause Button */
#define FORWARD_BTN       0xFFC23D    /* Forward Button    */
#define DOWN_BTN          0xFFE01F    /* Down Button       */
#define UP_BTN            0xFF906F    /* UP Button         */
#define EQ_BTN            0xFF9867    /* EQ Button         */
#define ST_REPT_BTN       0xFFB04F    /* ST/REPT Button    */
#define NUM_0_BTN         0xFF6897    /* Number 0 Button   */
#define NUM_1_BTN         0xFF30CF    /* Number 1 Button   */
#define NUM_2_BTN         0xFF18E7    /* Number 2 Button   */
#define NUM_3_BTN         0xFF7A85    /* Number 3 Button   */
#define NUM_4_BTN         0xFF10EF    /* Number 4 Button   */
#define NUM_5_BTN         0xFF38C7    /* Number 5 Button   */
#define NUM_6_BTN         0xFF5AA5    /* Number 6 Button   */
#define NUM_7_BTN         0xFF42BD    /* Number 7 Button   */
#define NUM_8_BTN         0xFF4AB5    /* Number 8 Button   */
#define NUM_9_BTN         0xFF52AD    /* Number 9 Button   */

/* DEFINE LED */
#define LED_PIN           6           /* Digital pin connected to LED output */
#define LED_MAX_VAL       255         /* LED max analog value */
#define LED_MIN_VAL       10          /* LED min analog value */
#define LED_CHANGE_VAL    0.3         /* Increase/Decrease LED brightness by a amount (10%,20%,...) */

int LED_value = 255;                        /* Store LED current value */

/* Create instance of 'irrecv' */
IRrecv irrecv( IR_RECV_PIN );

/* Create instance of 'decode_results' */
decode_results results;

void setup() {
  Serial.begin(9600);
  Serial.println( "Start IR Receiver Button Decode" );

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  /* Enable IR receiver module */
  irrecv.enableIRIn();
}

/********************************************************************************
 * Function Name: void control_LED( int cmd_code )
 * 
 * Parameters: int cmd_code  :  command code
 * 
 * Return: None.
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/30
 * 
 * Last Modified On: 2020/6/30
 * 
 * Description: Turns LED on/off or changes brightness.
 * 
 ********************************************************************************/
void control_LED( int cmd_code )
{
  switch ( cmd_code )
    {
      case OFF_BTN:
        digitalWrite(LED_PIN, !digitalRead(LED_PIN));
        break;
      case DOWN_BTN:
        if ( LED_value >= LED_MIN_VAL )
        {
          LED_value -= LED_CHANGE_VAL * LED_value;
          analogWrite( LED_PIN, LED_value );
        }
        else
        {
          analogWrite( LED_PIN, LED_MIN_VAL );
        }
        break;
      case UP_BTN:
        if ( ( LED_value + LED_CHANGE_VAL * LED_value ) <= LED_MAX_VAL )
        {
          LED_value += LED_CHANGE_VAL * LED_value;
          analogWrite( LED_PIN, LED_value );
        }
        else
        {
          analogWrite( LED_PIN, LED_MAX_VAL );
        }
        break;
      default:
        break;
    }
}

void loop()
{
  /* Check if received an IR signal */
  if ( irrecv.decode(&results) )
  {
    switch ( results.value )
    {
      case OFF_BTN:
        control_LED(OFF_BTN);
        Serial.println( "OFF button pressed." );
        break;
      case VOL_UP_BTN:
        Serial.println( "Volume up button pressed." );
        break;
      case FUNC_STOP_BTN:
        Serial.println( "Func/Stop button pressed." );
        break;
      case BACKWARD_BTN:
        Serial.println( "Backward button pressed." );
        break;
      case PLAY_PAUSE_BTN:
        Serial.println( "Play/Pause button pressed." );
        break;
      case FORWARD_BTN:
        Serial.println( "Forward button pressed." );
        break;
      case DOWN_BTN:
        control_LED(DOWN_BTN);
        Serial.println( "Down button pressed." );
        break;
      case VOL_DOWN_BTN:
        Serial.println( "Volume down button pressed." );
        break;
      case UP_BTN:
        control_LED(UP_BTN);
        Serial.println( "Up button pressed." );
        break;
      case EQ_BTN:
        Serial.println( "EQ button pressed." );
        break;
      case ST_REPT_BTN:
        Serial.println( "ST/REPT button pressed." );
        break;
      case NUM_0_BTN:
        Serial.println( "Number 0 button pressed." );
        break;
      case NUM_1_BTN:
        Serial.println( "Number 1 button pressed." );
        break;
      case NUM_2_BTN:
        Serial.println( "Number 2 button pressed." );
        break;
      case NUM_3_BTN:
        Serial.println( "Number 3 button pressed." );
        break;
      case NUM_4_BTN:
        Serial.println( "Number 4 button pressed." );
        break;
      case NUM_5_BTN:
        Serial.println( "Number 5 button pressed." );
        break;
      case NUM_6_BTN:
        Serial.println( "Number 6 button pressed." );
        break;
      case NUM_7_BTN:
        Serial.println( "Number 7 button pressed." );
        break;
      case NUM_8_BTN:
        Serial.println( "Number 8 button pressed." );
        break;
      case NUM_9_BTN:
        Serial.println( "Number 9 button pressed." );
        break;
      default:
         break;
    }

    /* Receive the next value */
    irrecv.resume();
  }
}
