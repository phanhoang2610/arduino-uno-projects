/********************************************************************************
 * File Name: 13_REAL_TIME_CLOCK_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/7
 * 
 * Last Modified On: 2020/7/7
 * 
 * Description: Test Real Time Clock Module (DS1307).
 *              
 ********************************************************************************/

#include <Wire.h>
#include <DS3231.h>

#define DELAY_TIME 1000

DS3231 clock;
RTCDateTime dt;

void setup() {
  Serial.begin( 9600 );
  
  Serial.println( "Initialize RTC module" );
  clock.begin();

  /* Sets datetime manually : (YYYY, MM, DD, HH, II, SS) */
  /* clock.setDateTime(2016, 12, 9, 11, 46, 00); */
  
  /* Send sketch compiling time to Arduino */
  clock.setDateTime( __DATE__, __TIME__ );
}

void loop() {
  dt = clock.getDateTime();

  /* Get and print Raw data */
  Serial.print( dt.year );
  Serial.print( "/" );
  Serial.print( dt.month );
  Serial.print( "/" );
  Serial.print( dt.day );
  Serial.print( " " );
  Serial.print( dt.hour );
  Serial.print( ":" );
  Serial.print( dt.minute );
  Serial.print( ":" );
  Serial.println( dt.second );

  delay( DELAY_TIME );
}
