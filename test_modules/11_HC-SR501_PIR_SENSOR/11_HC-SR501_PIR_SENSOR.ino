/********************************************************************************
 * File Name: 11_HC-SR501_PIR_SENSOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/6
 * 
 * Last Modified On: 2020/7/6
 * 
 * Description: Test HC-SR501 PIR Sensor.
 *              
 ********************************************************************************/

#define LED_PIN    8    /* LED on pin 8 of Arduino UNO */
#define PIR_PIN    7    /* PIR Sensor input pin 7  */

int pir_val; /* PIR Sensor pin value */

void setup() {
  pinMode( LED_PIN, OUTPUT );
  pinMode( PIR_PIN, INPUT );
  digitalWrite( LED_PIN, LOW );    /* Sets LED off */
}

void loop() {
  pir_val = digitalRead( PIR_PIN ); /* '0' if motion stopped, '1' if motion detected  */
  digitalWrite( LED_PIN, pir_val );
}
