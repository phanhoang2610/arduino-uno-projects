/********************************************************************************
 * File Name: 23_RC522_RFID_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/12
 * 
 * Last Modified On: 2020/7/12
 * 
 * Description: Test RC522 RFID Module. Gets and displays RFID Card's (or Tag) ID
 *              to Serial Monitor.
 * 
 ********************************************************************************/
 
#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN    10    /* Arduino's pin connected to SS pin  */
#define RST_PIN    9    /* Arduino's pin connected to RST pin */

#define PRIVATE_UID    "13 BD 06 03"    /* Change here the UID of the card/cards that you want to give access */

MFRC522 mfrc522( SS_PIN, RST_PIN );         /* Create MFRC522 instance */
 
void setup() 
{
  Serial.begin( 9600 );    /* Initiate a serial communication */
  SPI.begin();             /* Initiate SPI bus */
  mfrc522.PCD_Init();      /* Initiate MFRC522 */

  Serial.println( "Approximate your card/tag to the RFID reader..." );
}
void loop() 
{
  /* Look for new cards, and select one if present */
  if ( ! mfrc522.PICC_IsNewCardPresent() || ! mfrc522.PICC_ReadCardSerial() )
  {
    delay( 50 );
    return;
  }

  /* Show UID on serial monitor */
  Serial.print( "Card UID:" );
  String uid_str = "";

  /* Reads card/tag UID */
  for ( byte cnt = 0; cnt < mfrc522.uid.size; cnt++ ) 
  {
     uid_str += String( mfrc522.uid.uidByte[ cnt ] < 0x10 ? " 0" : " " );
     uid_str += String( mfrc522.uid.uidByte[ cnt ], HEX );
  }

  uid_str.toUpperCase();
  Serial.println(uid_str);

  /* Checks if approximated card/tag UID */
  if ( uid_str.substring( 1 ) == PRIVATE_UID )
  {
    Serial.println( "Access granted." );
    Serial.println();
    delay( 1000 );
  }
  else
  {
    Serial.println( "Access denied." );
    delay( 1000 );
  }

  /* Uncomment to dump the new memory contents */
  /*  Serial.println( F( "New UID and contents:" ) ); */
  /*  mfrc522.PICC_DumpToSerial( &( mfrc522.uid ) );  */
}
