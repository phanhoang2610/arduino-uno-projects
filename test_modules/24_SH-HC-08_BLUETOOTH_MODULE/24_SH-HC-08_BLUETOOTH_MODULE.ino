/********************************************************************************
 * File Name: 24_SH-HC-08_BLUETOOTH_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/13
 * 
 * Last Modified On: 2020/7/13
 * 
 * Description: Test SH-HC-08 Bluetooth 4.0 BLE Module.
 *              Uses smartphone (*) to connect to the SH-HC-08 Bluetooth module.
 *              Sends data from smartphone's bluetooth application (**) to control
 *              the LED. Sends 'ON' to turn LED on and 'OFF' to turn LED off.
 * 
 *              (*)  This project uses iPhone to test.
 *              (**) https://apps.apple.com/app/id1058693037 (Bluetooth Terminal)
 * 
 ********************************************************************************/

#include <SoftwareSerial.h>

#define LED_PIN     9
#define RX_PIN      10    /* Arduino's pin connected to HC-08 Module's TX Pin  */
#define TX_PIN      11    /* Arduino's pin connected to HC-08 Module's RX Pin  */

/* Defines bluetooth serial instance */
SoftwareSerial bt_serial( RX_PIN, TX_PIN );

/* Received data from other devices */
String received_data;

void setup() {
  Serial.begin( 9600 );
  pinMode( LED_PIN, OUTPUT );
  bt_serial.begin( 9600 );
}

void loop() {
  if ( bt_serial.available() )
  {
    /* Reads data */
    received_data = bt_serial.readString();

    /* Dispay received data to Arduino Serial Monitor */
    Serial.print( "Received:" );
    Serial.println( received_data );

    if ( received_data == "ON" )
    {
      digitalWrite( LED_PIN, HIGH );
    }

    if ( received_data == "OFF" )
    {
      digitalWrite( LED_PIN, LOW );
    }
  }
}
