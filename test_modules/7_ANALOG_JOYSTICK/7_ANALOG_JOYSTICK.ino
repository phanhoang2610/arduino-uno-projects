/********************************************************************************
 * File Name: 7_ANALOG_JOYSTICK.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/29
 * 
 * Last Modified On: 2020/6/29
 * 
 * Description: Test analog joystick module. When joystick: 
 *                +) Moves up, print "UP" to serial monitor and turn the GREEN LED on
 *                +) Moves down, print "DOWN" to serial monitor and turn the WHITE LED on
 *                +) Moves left, print "LEFT" to serial monitor and turn the BLUE LED on
 *                +) Moves right, print "RIGHT" to serial monitor and turn the RED LED on
 *              If switch clicked, turn all the LEDs off.
 * 
 ********************************************************************************/

/* DEFINE PINS */
#define JSTK_SW_PIN      2       /* Digital pin connected to switch output */
#define JSTK_X_PIN       0       /* Analog pin connected to X output */
#define JSTK_Y_PIN       1       /* Analog pin connected to Y output */

/* DEFINE JOYSTICK AXIS VALUES */
#define JSTK_XY_MID      500     /* Middle value of X,Y axis (~512)*/
#define JSTK_XY_MAX      1000    /* Max value of X,Y axis (~1024)*/
#define JSTK_XY_MIN      0       /* Min value of X,Y axis */

/* DEFINE LED PIN */
#define LEFT_LED_PIN     8       /* Digital pin connected to BLUE LED (LEFT) output */
#define RIGHT_LED_PIN    9       /* Digital pin connected to RED LED (RIGHT) output */
#define UP_LED_PIN       10      /* Digital pin connected to GREEN LED (UP) output */
#define DOWN_LED_PIN     11      /* Digital pin connected to WHITE LED (DOWN) output */


/* DEFINE LED ON/OFF BITMASK */
#define LEFT_LED_ON      8       /* BLUE LED (LEFT) turning on bitmask  ( 1000 ) */
#define RIGHT_LED_ON     1       /* RED LED (RIGHT) turning on bitmask  ( 0001 ) */
#define UP_LED_ON        4       /* GREEN LED (UP) turning on bitmask   ( 0100 ) */
#define DOWN_LED_ON      2       /* WHITE LED (DOWN) turning on bitmask ( 0010 ) */

int sw_state,    /* Switch pin output value */
    x_val,       /* X pin output value */
    y_val;       /* Y pin output value */

void setup() {
  /* Sets pins mode */
  pinMode(LEFT_LED_PIN,  OUTPUT);
  pinMode(RIGHT_LED_PIN, OUTPUT);
  pinMode(UP_LED_PIN,    OUTPUT);
  pinMode(DOWN_LED_PIN,  OUTPUT);
  pinMode(JSTK_SW_PIN, INPUT);
  
  digitalWrite(JSTK_SW_PIN, HIGH);
  
  Serial.begin(9600);
}

/********************************************************************************
 * Function Name: void turn_LED_on_off(int led_bit) 
 * 
 * Parameters: int led_bit  :  bitmask (use to define which LED turned on or off)
 * 
 * Return: None.
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/29
 * 
 * Last Modified On: 2020/6/29
 * 
 * Description: Turns one LED on, and turn other LEDs off.
 *              ( Example: RED LED ON -> GREEN, BLUE, WHITE LED OFF )
 * 
 ********************************************************************************/
void turn_LED_on_off(int led_bit)
{
  digitalWrite(LEFT_LED_PIN,  led_bit & LEFT_LED_ON);
  digitalWrite(RIGHT_LED_PIN, led_bit & RIGHT_LED_ON);
  digitalWrite(UP_LED_PIN,    led_bit & UP_LED_ON);
  digitalWrite(DOWN_LED_PIN,  led_bit & DOWN_LED_ON);
}

void loop() {
  /* Read joystick's X-axis pin value */
  x_val = analogRead(JSTK_X_PIN);
  
  /* Read joystick's Y-axis pin value */
  y_val = analogRead(JSTK_Y_PIN);
  
  /* Read joystick's switch pin value */
  sw_state = digitalRead(JSTK_SW_PIN);

  if ( ( x_val >= JSTK_XY_MAX ) && ( y_val >= JSTK_XY_MID ) )
  {
    Serial.println("UP");
    turn_LED_on_off(UP_LED_ON);
  }
  else if ( ( x_val <= JSTK_XY_MIN ) && ( y_val >= JSTK_XY_MID ) )
  {
    Serial.println("DOWN");
    turn_LED_on_off(DOWN_LED_ON);
  }
  else if ( ( x_val >= JSTK_XY_MID ) && ( y_val <= JSTK_XY_MIN ) )
  {
    Serial.println("LEFT");
    turn_LED_on_off(LEFT_LED_ON);
  }
  else if ( ( x_val >= JSTK_XY_MID ) && ( y_val >= JSTK_XY_MAX ) )
  {
    Serial.println("RIGHT");
    turn_LED_on_off(RIGHT_LED_ON);
  }
  else if ( sw_state == LOW )
  {
    Serial.println("CLEAR LEDs");
    turn_LED_on_off(0);
  }
}
