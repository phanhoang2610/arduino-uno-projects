/********************************************************************************
 * File Name: 6_DHT11_TEMPERATURE_AND_HUMIDITY_SENSOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/29
 * 
 * Last Modified On: 2020/6/29
 * 
 * Description: Test DHT11 Temperature and humidity sensor. Show the measured
 *              temperature and humidity to the serial monitor.
 * 
 ********************************************************************************/

#include <dht_nonblocking.h>

#define DHT_SENSOR_TYPE DHT_TYPE_11 /* DHT sensor type 11 */

static const int DHT_SENSOR_PIN = 2; /* DHT sensor connected pin : digital pin 2 */

DHT_nonblocking dht_sensor( DHT_SENSOR_PIN, DHT_SENSOR_TYPE ); /* Initialize the DHT sensor */

void setup( )
{
  Serial.begin( 9600); /* Initialize the serial port */
}

/********************************************************************************
 * Function Name: static bool measure_environment( float *temperature, 
 *                                                 float *humidity )
 * 
 * Parameters: float *temperature    temperature variable
 *             float *humidity       humidity variable
 * 
 * Return: Returns true if a measurement is available.
 *         Return false if a measurement is not available.
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/29
 * 
 * Last Modified On: 2020/6/29
 * 
 * Description: Poll for a measurement, keeping the state machine alive.
 *              Returns true if a measurement is available.
 * 
 ********************************************************************************/
static bool measure_environment( float *temperature, float *humidity )
{
  static unsigned long measurement_timestamp = millis( );

  /* Measure once every four seconds. */
  if ( millis( ) - measurement_timestamp > 3000ul ) /* ul: unsigned long */
  {
    /* Measure the temperature and  */
    if ( dht_sensor.measure( temperature, humidity ) == true )
    {
      measurement_timestamp = millis( );
      return ( true );
    }
  }

  return( false );
}

void loop( )
{
  float temperature;
  float humidity;

  /* Measure temperature and humidity. If the functions returns true, 
     then a measurement is available. */
  if ( measure_environment ( &temperature, &humidity ) == true )
  {
    Serial.print( "T = " );
    Serial.print( temperature, 1 );
    Serial.print( " *C, H = " );
    Serial.print( humidity, 1 );
    Serial.println( "%" );
  }
}
