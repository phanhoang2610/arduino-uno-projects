/********************************************************************************
 * File Name: 15_LCD_DISPLAY.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/7
 * 
 * Last Modified On: 2020/7/7
 * 
 * Description: Test LCD Display module. Use the real time clock module to get
 *              current date time. Then display current date to the LCD's row 1
 *              and display current time to the LCD's row 2.
 *              
 ********************************************************************************/

#include <LiquidCrystal.h>
#include <Wire.h>
#include <DS3231.h>

#define LCD_RS_PIN    7     /* LCD RS pin to digital pin 7 */
#define LCD_EN_PIN    8     /* LCD Enable pin to digital pin 8 */
#define LCD_D4_PIN    9     /* LCD D4 pin to digital pin 9 */
#define LCD_D5_PIN    10    /* LCD D5 pin to digital pin 10 */
#define LCD_D6_PIN    11    /* LCD D6 pin to digital pin 11 */
#define LCD_D7_PIN    12    /* LCD D7 pin to digital pin 12 */

#define LCD_COLS      16    /* LCD's number of columns */
#define LCD_ROWS      2     /* LCD's number of rows */

#define DELAY_TIME    1000

/* Define LiquidCrystal instance */
LiquidCrystal    lcd( LCD_RS_PIN, LCD_EN_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN );

/* Date/time module defines */
DS3231         clock;
RTCDateTime    dt;
char           str_dtime[ 12 ]; /* String date/time */

void clear_LCD_row( int row_no )
{
  /* Set the cursor to column 0, row 'row_no' */
  lcd.setCursor( 0, row_no );

  /* Clear all columns of row */
  for (  int col = 0 ; col < LCD_COLS ; col++)
  {
    lcd.print(" ");
  }
}

void setup() {
  clock.begin();

  /* Send sketch compiling time to Arduino */
  clock.setDateTime( __DATE__, __TIME__ );
  
  /* Set up the LCD with number of columns and rows */
  lcd.begin( LCD_COLS, LCD_ROWS );

  /* Print a string to the LCD */
  lcd.print( "Date:" );
}

void loop() {
  dt = clock.getDateTime();
  
  /**************** LCD's row 1 display ****************/
  /* Make the display's string */
  sprintf( str_dtime, "Date: %d-%d-%d", dt.year, dt.month, dt.day );

  /* Clear the row if month/day value changes from 2 digit to 1 digit */
  if ( ( dt.month == 1 ) || ( dt.day == 1 ) )
  {
    clear_LCD_row( 0 );
  }
  lcd.setCursor( 0, 0 );     /* Set the cursor to column 0, row 0 */
  lcd.print( str_dtime );    /* Print the string to the LCD display */

  /**************** LCD's row 2 display ****************/
  /* Make the display's string */
  sprintf( str_dtime, "Time: %02d:%02d:%02d", dt.hour, dt.minute, dt.second );

  /* Clear the row if hour/minute/second value changes from 2 digit to 1 digit */
  if ( ( dt.hour == 0 ) || ( dt.minute == 0 ) || ( dt.second == 0 ) )
  {
    clear_LCD_row( 1 );
  }
  lcd.setCursor( 0, 1 );
  lcd.print( str_dtime );
  
  delay( DELAY_TIME );
}
