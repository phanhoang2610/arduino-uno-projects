/********************************************************************************
 * File Name: 21_SERVO_MOTOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/11
 * 
 * Last Modified On: 2020/7/11
 * 
 * Description: Test servo motor.
 * 
 ********************************************************************************/

#include <Servo.h>

#define SERVO_PIN    9    /* Arduino's pin connected to servo motor */

Servo srv;    /* Create an object called srv */

void setup() {
  /* Attach the Servo variable to a pin */
  srv.attach( SERVO_PIN );

  /* Sets servo to 0 degree*/
  srv.write( 0 );
}

void loop() {
  srv.write( 0 );      /* Sets servo to 0 degree */
  delay( 2000 );       /* Waits 2 seconds */
  
  srv.write( 180 );    /* Sets servo to 180 degree */
  delay( 2000 );         /* Waits 2 seconds */
}
