/********************************************************************************
 * File Name: 22_THERMISTOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/11
 * 
 * Last Modified On: 2020/7/11
 * 
 * Description: Test thermistor. Uses thermistor to display to temperature.
 * 
 * Read more: https://github.com/panStamp/thermistor
 * 
 ********************************************************************************/

#include "thermistor.h"

#define THERMISTOR_PIN       A0       /* Arduino's pin connected to thermistor */

#define BCOEFFICIENT         3950     /* The beta coefficient of the thermistor (usually 3000-4000) */
#define SERIES_RESISTOR      10000    /* The 'other' resistor */
#define THERMISTORNOMINAL    10000    /* Nominal resistance at 25 ºC - Uses to calibrate reading data */

#define DELAY_TIME           5000

/* Defines thermistor object */
THERMISTOR thermistor( THERMISTOR_PIN,
                       THERMISTORNOMINAL,
                       BCOEFFICIENT,
                       SERIES_RESISTOR );

/* Define temperature reading */
int temperature;

void setup()
{
  Serial.begin( 9600 );
}

void loop()
{
  temperature = thermistor.read();    /* Read temperature */

  Serial.print( "***** Temperature : " );
  Serial.print( temperature / 10 );
  Serial.println( " ºC *****" );

  delay( DELAY_TIME );
}
