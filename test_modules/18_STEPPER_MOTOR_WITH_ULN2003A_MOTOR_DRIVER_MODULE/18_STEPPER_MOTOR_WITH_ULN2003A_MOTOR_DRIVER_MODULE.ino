/********************************************************************************
 * File Name: 18_STEPPER_MOTOR_WITH_ULN2003A_MOTOR_DRIVER_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/9
 * 
 * Last Modified On: 2020/7/9
 * 
 * Description: Test Stepper Motor (28BYJ-48) with ULN2003A Motor Driver Module.
 *
 ********************************************************************************/
 
#include <Stepper.h>

const int steps_per_revl  = 1000;    /* Number of steps per revolution (max = 2048) */
const int role_per_minute = 15;      /* Adjustable range of 28BYJ-48 stepper is 0~17 rpm */

const int in_1_pin = 11;             /* ULN2003A IC's IN 1 pin connected to Arduino */
const int in_2_pin = 10;             /* ULN2003A IC's IN 2 pin connected to Arduino */
const int in_3_pin = 9;              /* ULN2003A IC's IN 3 pin connected to Arduino */
const int in_4_pin = 8;              /* ULN2003A IC's IN 4 pin connected to Arduino */

/* Initialize the stepper library on pins 8 through 11 */
Stepper myStepper( steps_per_revl, in_4_pin, in_2_pin, in_3_pin, in_1_pin );

void setup() {
  /* Sets motor's speed (rpm) */
  myStepper.setSpeed( role_per_minute );
  Serial.begin( 9600 );
}

void loop() {  
  /* Step one revolution in one direction */
  Serial.println( "Counter-clockwise" );
  myStepper.step( steps_per_revl );
  delay( 1000 );

  /* Step one revolution in the other direction */
  Serial.println( "Clockwise" );
  myStepper.step( -steps_per_revl );
  delay( 1000 );
}
