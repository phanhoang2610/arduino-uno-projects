/********************************************************************************
 * File Name: 14_SOUND_SENSOR_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/7
 * 
 * Last Modified On: 2020/7/7
 * 
 * Description: Test Sound Sensor Module (KY-038).
 *              When sound detected, turn the LED on for 1 second.
 *              When sound not detected, turn the LED off.
 *              
 ********************************************************************************/

#define SS_ANALOG_PIN    A0    /* Sound sensor module analog pin connected to Arduino */
#define SS_DIGITAL_PIN   3     /* Sound sensor module digital pin connected to Arduino */
#define LED_PIN          7     /* LED pin connected to Arduino */

int analog_val = 0;     /* Sound sensor analog value */
int digital_val = 0;    /* Sound sensor digital value */

#define DELAY_TIME 1000

void setup() {
  Serial.begin( 9600 );

  pinMode( SS_ANALOG_PIN, INPUT );
  pinMode( SS_DIGITAL_PIN, INPUT );
  pinMode( LED_PIN, OUTPUT );
}
void loop() {
  digital_val = digitalRead( SS_DIGITAL_PIN );
  analog_val = analogRead( SS_ANALOG_PIN );

  /* Open Tools -> Serial Plotter to observe the waveforms changing */
  Serial.println( analog_val );

  if ( digital_val == HIGH )
  {
    digitalWrite( LED_PIN, HIGH );
    delay( DELAY_TIME );
  }
  else
  {
    digitalWrite( LED_PIN, LOW );
  }
}
