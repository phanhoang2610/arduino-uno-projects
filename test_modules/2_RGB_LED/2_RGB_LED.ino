/* DEFINE PINS */
#define RED_PIN          3
#define GREEN_PIN        5
#define BLUE_PIN         6

/* DEFINE FADING TIME BETWEEN COLOR */
#define DELAY_TIME       10

/* DEFINE MAX VALUE OF COLOR */
#define MAX_COLOR_VAL    255

/* Define color variables */
int redVal;
int greenVal;
int blueVal;

void setup()
{
  /* Set pin mode */
  pinMode(RED_PIN,   OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN,  OUTPUT);
}

void loop()
{
  redVal = MAX_COLOR_VAL;    /* Set red color to max value */
  greenVal = 0;
  blueVal = 0;

  /* Fades out red color and bring green color */
  for ( int i = 0 ; i < MAX_COLOR_VAL ; i++ )
  {
    redVal -= 1;
    greenVal += 1;
    analogWrite(RED_PIN, redVal);
    analogWrite(GREEN_PIN, greenVal);
    delay(DELAY_TIME);
  }

  redVal = 0;
  greenVal = MAX_COLOR_VAL;    /* Set green color to max value */
  blueVal = 0;

  /* Fades out green color and bring blue color */
  for ( int i = 0 ; i < MAX_COLOR_VAL; i++ )
  {
    greenVal -= 1;
    blueVal += 1;
    analogWrite(GREEN_PIN, greenVal);
    analogWrite(BLUE_PIN, blueVal);
    delay(DELAY_TIME);
  }

  redVal = 0;
  greenVal = 0;
  blueVal = MAX_COLOR_VAL;    /* Set blue color to max value */
  
  /* Fades out blue color and bring red color */
  for ( int i = 0 ; i < MAX_COLOR_VAL; i++ )
  {
    blueVal -= 1;
    redVal += 1;
    analogWrite(BLUE_PIN, blueVal);
    analogWrite(RED_PIN, redVal);
    delay(DELAY_TIME);
  }
}
