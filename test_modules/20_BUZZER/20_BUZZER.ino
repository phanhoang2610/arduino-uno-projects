/********************************************************************************
 * File Name: 20_BUZZER.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/11
 * 
 * Last Modified On: 2020/7/11
 * 
 * Description: Test buzzer (Active Buzzer OR Passive Buzzer).
 *              Use buzzer to play sounds and tones.
 *
 * Read more: https://ehub.ardusat.com/experiments/1756
 * 
 ********************************************************************************/

#define BUZZER_PIN    9    /* Arduino pin connected to buzzer's pin */

#define C4_NOTE    262    /* Piano C4 key frequency (Hz) */
#define D4_NOTE    294    /* Piano D4 key frequency (Hz) */
#define E4_NOTE    330    /* Piano E4 key frequency (Hz) */
#define F4_NOTE    349    /* Piano F4 key frequency (Hz) */
#define G4_NOTE    392    /* Piano G4 key frequency (Hz) */
#define A4_NOTE    440    /* Piano A4 key frequency (Hz) */
#define B4_NOTE    494    /* Piano B4 key frequency (Hz) */

#define C5_NOTE    523    /* Piano C5 key frequency (Hz) */
#define D5_NOTE    587    /* Piano D5 key frequency (Hz) */
#define E5_NOTE    659    /* Piano E5 key frequency (Hz) */
#define F5_NOTE    698    /* Piano F5 key frequency (Hz) */
#define G5_NOTE    784    /* Piano G5 key frequency (Hz) */
#define A5_NOTE    880    /* Piano A5 key frequency (Hz) */
#define B5_NOTE    988    /* Piano B5 key frequency (Hz) */

/* Defines a array which contains keys from C4 to C5 */
int test_notes[ 8 ] = { C4_NOTE, D4_NOTE, E4_NOTE, F4_NOTE, G4_NOTE, A4_NOTE, B4_NOTE ,C5_NOTE };

/********************************************************************************
 * Function Name: void make_sound_tone( long frequency, long duration )
 * 
 * Parameters: long frequency : Sound wave frequency in Hertz (Hz)
 *             long duration  : Sound wave play duration in Millisecond (ms)
 * 
 * Return: None.
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/11
 * 
 * Last Modified On: 2020/7/11
 * 
 * Description: Make a sound tone with sond wave frequency and play it in a duration.
 *              ( Humans can only hea within the frequency range of about 20Hz - 20,000Hz. 
 *                Dogs can hear much higher frequencies with a hearing range of 64Hz - 44,000Hz.)
 * 
 ********************************************************************************/

void make_sound_tone( long frequency, long duration )
{
  long delay_value = 1000000 / ( frequency * 2 );

  long number_cycles = frequency * duration / 1000;

  for ( long cnt = 0 ; cnt < number_cycles ; cnt++  )
  {
    digitalWrite( BUZZER_PIN, LOW );
    delayMicroseconds( delay_value );
    digitalWrite( BUZZER_PIN, HIGH );
    delayMicroseconds( delay_value );
  }
}

void setup()
{
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop()
{
  /* Plays the C4 to C5 key */
  for ( int cnt = 0 ; cnt < 8 ; cnt++ )
  {
    make_sound_tone( test_notes[ cnt ], 500 );       /* Plays key in 500 milliseconds */
    /* tone(BUZZER_PIN, test_notes[ cnt ], 500); */  /* Also can use Arduino's standard library function 'tone':
                                                        tone(pin, frequency, duration) */
    delay( 500 );                                    /* Wait 200 milliseconds */
  }
}
