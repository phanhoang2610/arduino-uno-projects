/********************************************************************************
 * File Name: 12_WATER_LEVEL_DETECTION_SENSOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/7
 * 
 * Last Modified On: 2020/7/7
 * 
 * Description: Test Water Level Detection Sensor Module.
 *              If water was detected, turn LED on. 
 *              If water was not detected, turn LED off
 *              
 ********************************************************************************/

#define WATER_SENSOR_PIN A0
#define LED_PIN          7

int water_sensor_val;

void setup() {
  pinMode( WATER_SENSOR_PIN, INPUT );
  pinMode( LED_PIN, OUTPUT );
  digitalWrite( LED_PIN, LOW );
}

void loop() {
  water_sensor_val = analogRead( WATER_SENSOR_PIN );
  
  /* Water is detected */
  if ( water_sensor_val > 10 )
  {
    digitalWrite( LED_PIN, HIGH );  
  }
  else
  {
    digitalWrite( LED_PIN, LOW );
  }
}
