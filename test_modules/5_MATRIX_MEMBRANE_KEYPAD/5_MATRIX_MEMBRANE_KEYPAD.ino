/********************************************************************************
 * File Name: 5_MATRIX_MEMBRANE_KEYPAD.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/28
 * 
 * Last Modified On: 2020/6/28
 * 
 * Description: Test 4x4 Matrix membrane keypad. Simple password check program.
 *              If entered password is true, turn the green LED on, otherwise
 *              turn the green LED off and turn the red LED on.
 * 
 ********************************************************************************/

#include <Keypad.h>

/* Set pins */
const byte KEYPAD_ROWS   = 4;     /* Four rows */
const byte KEYPAD_COLS   = 4;     /* Four columns */

const byte GREEN_LED_PIN = 10;    /* Green LED: digital pin 10 */
const byte RED_LED_PIN   = 11;    /* Red LED: digital pin 11*/

/* Define the two-dimensional array on the buttons of the keypads */
char hexaKeys[KEYPAD_ROWS][KEYPAD_COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
/* Connect to the row pinouts of the keypad */
byte rowPins[KEYPAD_ROWS] = {9, 8, 7, 6};

/* Connect to the column pinouts of the keypad */
byte colPins[KEYPAD_COLS] = {5, 4, 3, 2};

/* Initialize an instance of class NewKeypad */
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, KEYPAD_ROWS, KEYPAD_COLS);

/* Password string */
String pwd;

void setup() {
  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  
  Serial.begin(9600);
  Serial.println("Enter password: ");
}

void loop() {
  /* Read pressed key from keypad */
  char customKey = customKeypad.getKey();
  
  switch (customKey)
  {
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      if ( pwd.length() < 4 )
      {
        pwd += customKey;
        Serial.println(pwd);
      }
      else
      {
        Serial.println("Press button \'A\'");
      }
      break;
    case 'A':
      if ( pwd == "9999" )
      {
          Serial.println("Access granted!");
          digitalWrite(GREEN_LED_PIN, HIGH);
          digitalWrite(RED_LED_PIN, LOW);
      }
      else
      {
        Serial.println("Wrong Password!");
        Serial.println("Re-enter password: ");
        digitalWrite(GREEN_LED_PIN, LOW);
        digitalWrite(RED_LED_PIN, HIGH);
      }
      pwd = "";
      break;
    default:
      break;
  }
}
