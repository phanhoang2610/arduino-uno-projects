/* DEFINE PINS */
#define LED_PIN   13
#define BTN_1_PIN 8
#define BTN_2_PIN 9

void setup() {
  /* Set pin mode for LED */
  pinMode(LED_PIN, OUTPUT);

  /* Set pin mode pullup for LED */
  pinMode(BTN_1_PIN, INPUT_PULLUP);
  pinMode(BTN_2_PIN, INPUT_PULLUP);
}

void loop() {
  /* Press button 1 for turning on LED */
  if ( digitalRead(BTN_1_PIN) == LOW )
  {
    digitalWrite(LED_PIN, HIGH);
  }

  /* Press button 2 for turning off LED */
  if ( digitalRead(BTN_2_PIN) == LOW )
  {
    digitalWrite(LED_PIN, LOW);
  }
}
