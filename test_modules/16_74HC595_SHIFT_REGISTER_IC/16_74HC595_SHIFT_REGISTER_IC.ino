/********************************************************************************
 * File Name: 16_74HC595_SHIFT_REGISTER_IC.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/8
 * 
 * Last Modified On: 2020/7/8
 * 
 * Description: Test 74HC595 Shift Register IC (with 8 LEDs).
 *              Turns LED number 1 to 8 on , then turns LED number 8 to 1 off.
 *
 ********************************************************************************/

#define CLOCK_PIN    9     /* SH_CP [SCK] on 74HC595 connected to Arduino pin 9 */
#define LATCH_PIN    11    /* ST_CP [RCK] on 74HC595 connected to Arduino pin 11 */
#define DATA_PIN     12    /* DS [S1] on 74HC595 connected to Arduino pin 12 */

#define DELAY_TIME    100

byte leds_bit = 0;    /* Hold the pattern of which LEDs are currently turned on or off */

/* Using 74CH595 :
 *   (1) Set latch pin to LOW -> Begin transmitting signals.
 *   (2) Shift out the bit -> Uses shiftOut( DATA_PIN, CLOCK_PIN, bitOrder, value ) function
 *        > DATA_PIN ( integer ): The pin on which to output each bit.
 *        > CLOCK_PIN  ( integer ): The pin to toggle once the DATA_PIN has been set to the correct value.
 *        > bitOrder: Which order to shift out the bits ( MSBFIRST or LSBFIRST ).
 *                    MSBFIRST - Most Significant Bit First
 *                    LSBFIRST - Least Significant Bit First
 *        > value ( byte ): The data to shift out.
 *   (3) Set latch pin to HIGH -> The end of the transmission signal.
 */

void change_shift_register()
{
  digitalWrite( LATCH_PIN, LOW );
  shiftOut( DATA_PIN, CLOCK_PIN, LSBFIRST, leds_bit );    /* Update shift register */
  digitalWrite( LATCH_PIN, HIGH );
}

void setup() 
{
  pinMode( LATCH_PIN, OUTPUT );
  pinMode( DATA_PIN,  OUTPUT );  
  pinMode( CLOCK_PIN, OUTPUT );
}

void loop() 
{
  /* Turns LED 1->8 ON */
  for ( int cnt = 0 ; cnt < 8 ; cnt++ )
  {
    leds_bit |= ( 1 << cnt );        /* Make a set of '1' bit with length 'cnt + 1' (Ex: 1,11,111,...) */
    /* bitSet( leds_bit, cnt ) */    /* Same method: using bitSet( leds_bit, cnt ) function to make set of '1' bit */
    change_shift_register();         /* Update shift register */
    delay( DELAY_TIME );
  }

  /* Turns LED 8->1 OFF */
  for ( int cnt = 0 ; cnt < 8 ; cnt++ )
  {
    leds_bit = 255 >> ( cnt + 1 );    /* B1111111 -> B01111111 -> B00111111 ... */
    change_shift_register();          /* Update shift register */
    delay( DELAY_TIME );
  }
}
