/********************************************************************************
 * File Name: 4_TILT_BALL_SWITCH.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/6/28
 * 
 * Last Modified On: 2020/6/28
 * 
 * Description: Test tilt ball switch module (SW-520D) with LED. If tilted, turns 
 *              the LED on, otherwise turns the LED off.
 *              
 ********************************************************************************/
 
/* DEFINE PINS */
#define TILT_SW_PIN    2    /* Tilt ball switch connected to digital pin 2*/
#define LED_PIN        6    /* LED PIN connected to digital pin 6 */

int digitVal = 0;           /* Digital value at pin 2 (tilt switch) */

void setup() {
  /* Sets LED digital pin as output */
  pinMode(LED_PIN, OUTPUT);
  /* Sets tilt ball switch digital pin as input */
  pinMode(TILT_SW_PIN, INPUT);
  /* Turn the LED on */
  digitalWrite(2, HIGH);
}

void loop() {
  /* Read tilt ball switch digital pin */
  digitVal = digitalRead(TILT_SW_PIN);

  if ( digitVal == HIGH )
  {
    /* Turn the LED off */
    digitalWrite(LED_PIN, LOW);
  }
  else
  {
    /* Turn the LED on */
    digitalWrite(LED_PIN, HIGH);
  }
}
