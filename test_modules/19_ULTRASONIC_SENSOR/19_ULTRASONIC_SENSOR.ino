/********************************************************************************
 * File Name: 18_STEPPER_MOTOR_WITH_ULN2003A_MOTOR_DRIVER_MODULE.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/10
 * 
 * Last Modified On: 2020/7/10
 * 
 * Description: Test Ultrasonic Sensor (HC-SR04). Calculates the distance between
 *              the HC-SR04 sensor and an object directly infront of it.
 *
 ********************************************************************************/

#define TRIGGER_PIN    13    /* Ultrasonic Sensor Trigger Pin */
#define ECHO_PIN       12    /* Ultrasonic Sensor Echo Pin */

#define MAX_SENSOR_DISTANCE    200
#define MIN_SENSOR_DISTANCE    0

#define DELAY_TIME    500

float duration, distance;

void setup() {
  Serial.begin ( 9600 );
  pinMode( TRIGGER_PIN, OUTPUT );
  pinMode( ECHO_PIN, INPUT );
}

void loop() {
  digitalWrite( TRIGGER_PIN, LOW ); 
  delayMicroseconds( 2 ); 

  /* Generate 10-microsecond pulse to TRIGGER pin */
  digitalWrite( TRIGGER_PIN, HIGH );
  delayMicroseconds( 10 );

  digitalWrite( TRIGGER_PIN, LOW );

  /* Measure duration of pulse from ECHO pin */
  duration = pulseIn( ECHO_PIN, HIGH );

  /* Calculate the distance :
   * Travel distance = Sound speed × Travel Time = 0.034 (1/29.1) × Pulse Duration
   */
  distance = ( duration / 2 ) / 29.1;

  if ( ( distance >= MAX_SENSOR_DISTANCE ) || ( distance <= MIN_SENSOR_DISTANCE ) )
  {
    Serial.println( "Out of range" );
  }
  else
  {
    Serial.print( distance );
    Serial.println( " cm" );
  }

  delay( DELAY_TIME );
}
