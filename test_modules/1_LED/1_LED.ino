#define LED_PIN       13     /* Arduino pin connected to LED's anode (+) pin */
#define DELAY_TIME    200    /* Delay time (in milliseconds) */

void setup() {
  /* Set pin to output mode */
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  /* Set HIGH value to pin -> turn ON the LED */
  digitalWrite(LED_PIN, HIGH);
  
  /* Pauses the program */
  delay(DELAY_TIME);
  
  /* Set HIGH value to pin -> turn OFF the LED */
  digitalWrite(LED_PIN, LOW);

  /* Pauses the program */
  delay(DELAY_TIME);
}
