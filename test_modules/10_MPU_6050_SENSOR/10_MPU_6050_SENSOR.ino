/********************************************************************************
 * File Name: 10_MPU_6050_SENSOR.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/6
 * 
 * Last Modified On: 2020/7/6
 * 
 * Description: Test MPU-6050 Accelerometer and Gyroscope sensor module.
 *              Tutorial:
 *                  https://youtu.be/M9lZ5Qy5S2s
 *              MPU-6050's Datasheet: 
 *                  https://invensense.tdk.com/products/motion-tracking/6-axis/mpu-6050/
 * 
 ********************************************************************************/

#include <Wire.h>

#define MPU_ADDR         0x68    /* I2C address of the MPU-6050 */
#define PWR_MGMT_1       0x6B    /* Power Management Register Address */
#define GYRO_CFG         0x1B    /* Gyroscope Configuration Register Address */
#define ACCEL_CFG        0x1C    /* Acccelerometer Configuration Register Address */
#define ACCEL_START      0x3B    /* Accel readings starting register */
#define GYRO_START       0x43    /* Gyro readings starting register */

#define DELAY_TIME       500

const float accel_sensi = 16384.0;    /* Accelerometer’s sensitivity per LSB in scale full range ±2g*/
const float gyro_sensi  = 131.0;      /* Gyroscope’s sensitivity per LSB in scale full range ± 250°/s*/

long accel_X, accel_Y, accel_Z;
float gForce_X, gForce_Y, gForce_Z;

long gyro_X, gyro_Y, gyro_Z;
float rotat_X, rotat_Y, rotat_Z;

void setup_MPU_sensor(long int addr) {
  Wire.beginTransmission( MPU_ADDR );
  Wire.write( addr );
  Wire.write( 0x0 );
  Wire.endTransmission();  
}

void record_accel_registers() {
  Wire.beginTransmission(MPU_ADDR); /* I2C address of the MPU */
  Wire.write(ACCEL_START);          /* Starting register for Accel Readings */
  Wire.endTransmission();
  Wire.requestFrom(MPU_ADDR, 6);    /* Request Accel Registers (3B - 40) */

  while(Wire.available() < 6);      /* Check data is available for retrieval  */
  
  accel_X = ( Wire.read() << 8 ) | Wire.read(); /* Store first two bytes into accel_X */
  accel_Y = ( Wire.read() << 8 ) | Wire.read(); /* Store middle two bytes into accel_Y */
  accel_Z = ( Wire.read() << 8 ) | Wire.read(); /* Store last two bytes into accel_Z */
  processAccelData();
}

void processAccelData(){
  gForce_X = accel_X / accel_sensi;
  gForce_Y = accel_Y / accel_sensi; 
  gForce_Z = accel_Z / accel_sensi;
}

void record_gyro_registers() {
  Wire.beginTransmission(MPU_ADDR); /* I2C address of the MPU */
  Wire.write(GYRO_START);           /* Starting register for Gyro Readings */
  Wire.endTransmission();
  Wire.requestFrom(MPU_ADDR, 6);    /* Request Gyro Registers (43 - 48) */
  
  while( Wire.available() < 6 );    /* Check data is available for retrieval  */
  
  gyro_X = ( Wire.read() << 8 ) | Wire.read(); /* Store first two bytes into accel_X */
  gyro_Y = ( Wire.read() << 8 ) | Wire.read(); /* Store middle two bytes into accel_Y */
  gyro_Z = ( Wire.read() << 8 ) | Wire.read(); /* Store last two bytes into accel_Z */
  processGyroData();
}

void processGyroData() {
  rotat_X = gyro_X / gyro_sensi;
  rotat_Y = gyro_Y / gyro_sensi; 
  rotat_Z = gyro_Z / gyro_sensi;
}

void print_data() {
  Serial.print( "Gyro (deg)" );
  Serial.print( " X=" );
  Serial.print( rotat_X );
  Serial.print( " Y=" );
  Serial.print( rotat_Y );
  Serial.print( " Z=" );
  Serial.print( rotat_Z );
  Serial.print( " Accel (g)" );
  Serial.print( " X=" );
  Serial.print( gForce_X );
  Serial.print( " Y=" );
  Serial.print( gForce_Y );
  Serial.print( " Z=" );
  Serial.println( gForce_Z );
}

void setup() {
  Serial.begin( 9600 );

  Wire.begin();

  /* Accessing the Power Management register*/
  setup_MPU_sensor( PWR_MGMT_1 );

  /* Accessing the Gyroscope Configuration register */
  setup_MPU_sensor( GYRO_CFG );

  /* Accessing the Acccelerometer Configuration register */
  setup_MPU_sensor( ACCEL_CFG );
}

void loop() {
  record_accel_registers();
  record_gyro_registers();
  print_data();
  delay( DELAY_TIME );
}
