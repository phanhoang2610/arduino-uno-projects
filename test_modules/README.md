# Modules Learning With Arduino UNO R3 (ATmega328P)
1. LED
2. RBG LED
3. Digital Inputs (Push Buttons and LED)
4. Tilt ball switch (SW520D) with LED
5. Matrix membrane keypad with LED
6. DHT11 Temperature and Humidity Sensor
7. Analog joystick (with LEDs)
8. IR Receiver (with LED)
9. MAX7219 LED dot matrix (8x8) module
10. GY-521 (MPU-6050) Accelerometer and Gyroscope Sensor
11. HC-SR501 PIR Sensor
12. Water Level Detection Sensor Module
13. Real time clock module (DS1307)
14. Sound sensor module (KY-038)
15. LCD Display Module (with Real time clock module (DS1307))
16. 74HC595 Shift Register IC (with 8 LEDs)
17. DC Motor with Motor Driver L293D IC
18. Stepper Motor (28BYJ-48) with ULN2003A Motor Driver Module
19. Ultrasonic Sensor (HC-SR04)
20. Passive Buzzer and Active Buzzer
21. Servo Motor
22. Thermistor
23. RC522 RFID Module
24. SH-HC-08 Bluetooth 4.0 BLE Module