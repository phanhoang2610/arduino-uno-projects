//
//  ViewController.swift
//  RemoteDoor Control
//
//  Created by Phan Hoang on 2020/07/26.
//  Copyright © 2020 Phan Hoang. All rights reserved.
//

import UIKit

import CoreBluetooth

class ViewController: UIViewController, CBPeripheralDelegate, CBCentralManagerDelegate {    /* Adds CBPeripheralDelegate, CBCentralManagerDelegate */
    
    // ///////////////////////////////////////////////////////////// //
    //                                                               //
    //                       Variables Declaration                   //
    //                                                               //
    // ///////////////////////////////////////////////////////////// //

    @IBOutlet weak var btn_connect: UIButton!
    
    @IBOutlet weak var btn_open: UIButton!
    
    @IBOutlet weak var btn_close: UIButton!
    
    @IBOutlet weak var lbl_status: UILabel!
    
    /* Declares variables to store the actual central manager and peripheral */
    private var central_manager: CBCentralManager!
    private var peripheral: CBPeripheral!
    var mainCharacteristic:CBCharacteristic? = nil
    
    let MY_DEVICE_NAME = "SH-HC-08"
    let MY_MAIN_BLE_CHARACTERISTIC = "FFE1"
    
    /* Bluetooth status (ON or OFF) */
    var manager:CBCentralManager!
    
    // ///////////////////////////////////////////////////////////// //
    //                                                               //
    //             Bluetooth Connect Event Functions                 //
    //                                                               //
    // ///////////////////////////////////////////////////////////// //
    
    /* Updates when the Bluetooth Peripheral is switched on or off function */
    func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        print("----- Central state update -----") // DEBUG
        if (central.state != .poweredOn)
        {
            print("Central is not powered on") // DEBUG
            lbl_status.text = "Please turn Bluetooth on"
            self.setButtonsNotConnected()
            self.btn_connect.isEnabled = false
        }
        else
        {
            central_manager.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    /* Tells the delegate the central manager discovered a peripheral while scanning for devices */
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {
        print("----- Central Manager didDiscover -----") // DEBUG
        
        let devices = (advertisementData as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey) as? NSString
                
        if (devices?.contains(MY_DEVICE_NAME) == true)
        {
            self.central_manager.stopScan()
            
            self.peripheral = peripheral
            self.peripheral.delegate = self
            
            central_manager.connect(peripheral, options: nil)
            self.stopScanForBLEDevices()
            
            self.lbl_status.text = "Connected"
            
            self.setButtonsConnected()
                        
        }
    }
    
    /* Tells the delegate that the central manager connected to a peripheral */
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
    {
        print("----- Central Manager didConnect -----") // DEBUG
        peripheral.discoverServices(nil)
        print("My bluetooth module UUID: ", peripheral.identifier.uuidString)
    }
    
    /* Tells the delegate that peripheral service discovery succeeded */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?)
    {
        print("----- didDiscoverServices -----") // DEBUG
        for service in peripheral.services!
        {
            print("Found service: ", service.uuid.uuidString)
            peripheral.discoverCharacteristics(nil, for: service)
            
        }
    }
    
    /* Tells the delegate that the peripheral found characteristics for a service */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?)
    {
        for characteristic  in service.characteristics!
        {
            print(characteristic.uuid, ": ", characteristic as Any) // DEBUG
            
            if (characteristic.uuid.uuidString == "FFE1")
            {
                peripheral.readValue(for: characteristic)
                mainCharacteristic = characteristic
                peripheral.setNotifyValue(true, for: characteristic)
            }
        }
        
    }
    
    /* Tells the delegate that retrieving the specified characteristic’s value succeeded, or that the characteristic’s value changed */
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?)
    {
        print("----- didUpdateValueFor -----") // DEBUG
        print(characteristic) // DEBUG
        if (characteristic.uuid.uuidString == "FFE1")
        {
            let str_value = String(data: characteristic.value!, encoding: .utf8)
            print(str_value as Any) // DEBUG
        }
    }
    
    /* Tells the delegate that the central manager disconnected from a peripheral */
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?)
    {
        if (peripheral.name == MY_DEVICE_NAME)
        {
            self.lbl_status.text = "Not connected"
            self.setButtonsNotConnected()
        }
    }
    
    
    // ///////////////////////////////////////////////////////////// //
    //                                                               //
    //                       viewDidLoad Function                    //
    //                                                               //
    // ///////////////////////////////////////////////////////////// //
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        manager = CBCentralManager()
        manager.delegate = self
        
        /* Init the centralManager --- */
        central_manager = CBCentralManager(delegate: self, queue: nil)
        
        self.lbl_status.textColor = .black
        
        self.btn_connect.layer.borderWidth = 2.0
        self.btn_connect.layer.cornerRadius = btn_connect.frame.size.width/2
        
        self.btn_open.layer.borderWidth = 2.0
        self.btn_open.layer.cornerRadius = btn_open.frame.size.width/2
        
        self.btn_close.layer.borderWidth = 2.0
        self.btn_close.layer.cornerRadius = btn_close.frame.size.width/2
                
        self.setButtonsNotConnected()
    }
    
    
    // ///////////////////////////////////////////////////////////// //
    //                                                               //
    //                Button Touched Event Functions                 //
    //                                                               //
    // ///////////////////////////////////////////////////////////// //
    
    @IBAction func btn_connect_devices_touched(_ sender: UIButton)
    {
        
        btn_connect.isEnabled = false
        
        if manager.state != .poweredOn
        {
            print("----- Central is not powered on -----") // DEBUG
        }
        else
        {
            central_manager.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    @IBAction func btn_open_touched(_ sender: UIButton)
    {
        self.btn_open.isEnabled = false
        self.btn_close.isEnabled = false
        self.peripheral?.writeValue("on".data(using: String.Encoding.utf8)!, for: mainCharacteristic!, type: .withoutResponse)
        self.lbl_status.text = "... Please wait ..."

        /* Wait 3 seconds */
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(enableOpenButton), userInfo: nil, repeats: false)
    }

    @objc func enableOpenButton() {
        self.lbl_status.text = "Door is opened"
        self.btn_open.isEnabled = true
        self.btn_close.isEnabled = true
    }
    
    @IBAction func btn_close_touched(_ sender: UIButton)
    {
        self.btn_close.isEnabled = false
        self.btn_open.isEnabled = false
        self.peripheral?.writeValue("off".data(using: String.Encoding.utf8)!, for: mainCharacteristic!, type: .withoutResponse)
        self.lbl_status.text = "... Please wait ..."

        /* Wait 3 seconds */
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(enableCloseButton), userInfo: nil, repeats: false)
    }

    @objc func enableCloseButton()
    {
        self.lbl_status.text = "Door is closed"
        self.btn_close.isEnabled = true
        self.btn_open.isEnabled = true
    }
    
    // ///////////////////////////////////////////////////////////// //
    //                                                               //
    //                      Utilities Functions                      //
    //                                                               //
    // ///////////////////////////////////////////////////////////// //
    
    func stopScanForBLEDevices()
    {
        manager?.stopScan()
    }
    
    func setButtonsNotConnected()
    {
        btn_connect.isEnabled = true
        btn_connect.layer.borderColor = UIColor.blue.cgColor
        btn_connect.setTitleColor(UIColor .blue, for: .normal)

        btn_open.isEnabled = false
        btn_open.layer.borderColor = UIColor.gray.cgColor
        btn_open.setTitleColor(UIColor .gray, for: .normal)
        
        btn_close.isEnabled = false
        btn_close.layer.borderColor = UIColor.gray.cgColor
        btn_close.setTitleColor(UIColor .gray, for: .normal)
    }
    
    func setButtonsConnected()
    {
        btn_connect.isEnabled = false
        btn_connect.layer.borderColor = UIColor.gray.cgColor
        btn_connect.setTitleColor(UIColor .gray, for: .normal)

        btn_open.isEnabled = true
        btn_open.layer.borderColor = UIColor.orange.cgColor
        btn_open.setTitleColor(UIColor .orange, for: .normal)
        
        btn_close.isEnabled = true
        btn_close.layer.borderColor = UIColor.black.cgColor
        btn_close.setTitleColor(UIColor .black, for: .normal)
    }
    
}



