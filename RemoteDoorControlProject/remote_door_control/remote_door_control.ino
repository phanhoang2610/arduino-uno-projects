/********************************************************************************
 * File Name: remote_door_control.ino
 * 
 * File Type: Arduino Sketch
 * 
 * Author: Phan Hoang
 * 
 * Created On: 2020/7/13
 * 
 * Last Modified On: 2020/7/13
 * 
 * Description: When bluetooth module receives 'on' message, sets servo to ~90º to open the door lock.
 *              When bluetooth module receives 'off' message, sets servo to 0º to close the door lock.
 * 
 ********************************************************************************/
 
#include <SoftwareSerial.h>
#include <Servo.h>
 
#define SERVO_PIN     9

#define RX_PIN        3    /* Arduino's pin connected to HC-08 Module's TX Pin  */
#define TX_PIN        4    /* Arduino's pin connected to HC-08 Module's RX Pin  */

/* Defines bluetooth serial instance */
SoftwareSerial bt_serial( RX_PIN, TX_PIN );

/* Create an object called srv */
Servo srv;

/* Received data from other devices */
String received_data;

void setup() {
  bt_serial.begin( 9600 );
//  Serial.begin(9600);    // Debug Only

  /* Turns LED 13 off for saving energy */
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  /* Attach the Servo variable to a pin */
  srv.attach( SERVO_PIN );

  /* Sets servo to 0 degree */
  srv.write( 0 );
}

void loop() {
  if ( bt_serial.available() )
  {
    /* Reads current position of servo */
    int curr_pos = srv.read();

    int from_pos = 0;
    
    /* Reads data */
    received_data = bt_serial.readString();

    /* Display received data to Arduino Serial Monitor */
//    Serial.print( "Received:" );        // Debug Only
//    Serial.println( received_data );    // Debug Only

    /* If received data is a string with value 'on', then sets servo to 90~95* */
    if ( received_data == "on" )
    {
      if ( curr_pos >=0 )
      {
        from_pos = curr_pos;
      }

      /* Sets door lock open */
      for( int angl = from_pos ; angl <= 95 ; angl++ )
      {
        srv.write( angl );
        delay( 5 );
      }
    }

    /* If received data is a string with value 'off', then sets servo to 0* */
    if ( received_data == "off" )
    {
      if ( curr_pos <= 95 )
      {
        from_pos = curr_pos;
      }

      /* Sets door lock close */
      for( int angl = from_pos ; angl >= 0 ; angl-- )
      {
        srv.write( angl );
        delay( 5 );
      }
    }
  }
}
