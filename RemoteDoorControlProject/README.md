# Project Description

## Project Name 
Remote Door Lock Control using Bluetooth

## Product Demo Video
[Youtube Link](https://youtu.be/zVRBBQJyxr0)
[LinkedIn Link](https://www.linkedin.com/posts/phan-hoang-b8451a18b_arduinoproject-arduinouno-bluetoothlowenergy-activity-6696757460939751424-nFOg/)

## Project Purpose
Locking/Unlocking the door remotely using iOS application.

## Hardware and software requirements

1. Hardware requirements:
 -  Arduino Uno R3 (1)
 -  SH-HC-08 Bluetooth BLE Module (1)
 -  MicroServo SG90 (1)
 -  Others parts: 9v Battery (1), AA Battery (4), 4xAA Battery Holder(1), Wood board(1), Small round magnets(7)

2. Software requirements:
 -  Arduino IDE (1.8.13)
 -  XCode IDE (11.6)
 -  MacOS (10.15.5)

## iOS Application Description
1. App Name: [RemoteDoorLock](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/RemoteDoorLock)
2. App Description: This app is used for connecting iPhone to the bluetooth module and sending commands. This app was built with Swift programming language and XCode IDE on MacOS.

## Arduino Program 
1. Program Name: [remote_door_control](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/remote_door_control)
2. Program Description: This program is used for reading/processing the data received from bluetooth module and controlling servo motor. This program was built with Arduino IDE.

## 3D Design

[Remote Door Control 3D](https://www.tinkercad.com/things/eZ2uCEdEL0q)

## Sample Images
[Link](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/SampleImages)
1. [iOS App](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/blob/master/RemoteDoorControlProject/SampleImages/iOS_App.jpeg)
2. [Door Lock Module](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/blob/master/RemoteDoorControlProject/SampleImages/Door_Lock_Control_Module.jpg)

# _____________________________________________________________________________________

# プロジェクトの概要

## プロジェクト名 
Remote Door Lock Control using Bluetooth

## 参考動画

[Youtube Link](https://youtu.be/zVRBBQJyxr0)
[LinkedIn Link](https://www.linkedin.com/posts/phan-hoang-b8451a18b_arduinoproject-arduinouno-bluetoothlowenergy-activity-6696757460939751424-nFOg/)

## プロジェクトの目的
このプロジェクトの目的は、iOSアプリケーションを使用してドアをリモートでロックおよびロック解除することです。


## ハードウェアおよびソフトウェアの要件

1. ハードウェア要件：
 -  Arduino Uno R3（1）
 -  SH-HC-08 Bluetooth BLEモジュール（1）
 -  MicroServo SG90（1）
 -  その他のパーツ：9vバッテリー（1）、AAバッテリー（4）、4xAAバッテリーホルダー（1）、木板（1）、小さな丸い磁石（7）

2. ソフトウェア要件：
 -  Arduino IDE（1.8.13）
 -  XCode IDE（11.6）
 -  MacOS（10.15.5）

## iOSアプリケーションの説明
1. アプリ名：[RemoteDoorLock](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/RemoteDoorLock)
2. アプリの説明：このアプリは、iPhoneをBluetoothモジュールに接続し、コマンドを送信するために使用します。このアプリは、MacOSのSwiftプログラミング言語とXCode IDEを使用しています。

## Arduinoプログラム
1. プログラム名：[remote_door_control](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/remote_door_control)
2. プログラムの説明：このプログラムは、Bluetoothモジュールから受信したデータの読み取り/処理とサーボモーターの制御に使用します。このプログラムはArduino IDEでビルドしました。

## 3Dデザイン

[リモートドアコントロール3D](https://www.tinkercad.com/things/eZ2uCEdEL0q)

## サンプル画像
[Link](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/tree/master/RemoteDoorControlProject/SampleImages)
1. [iOS App](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/blob/master/RemoteDoorControlProject/SampleImages/iOS_App.jpeg)
2. [Door Lock Module](https://gitlab.com/phanhoang2610/arduino-uno-projects/-/blob/master/RemoteDoorControlProject/SampleImages/Door_Lock_Control_Module.jpg)


